var assert = require('assert');
var should = require('should');
var logger = require('winston');
var api = require('../vspapi.js');
var cards = require('./sampleCards.js');
var ctx = require('./sampleCtx.js');

api.config(ctx);

logger.level = "debug";

describe('Token Test Suite', function() {
    this.timeout(35000);
    
    before(function(done){
         api.queryToken({
            "firstName": cards.visa1.cardholder.firstName,
            "lastName" : cards.visa1.cardholder.lastName,
            "expiration" : cards.visa1.expiration,
            // "cardType" : "Visa"
            
        }, function(reply){
            if (reply.status == "1") {
                done();
            } else {
                var results = reply.results;
                
                for(var ndx=0; ndx<results.length; ndx++) {
                    api.deleteToken(results[ndx].token, function(reply){
                        done();
                    });
                }
            }

        });
    });
    
    it('can create, update, query and delete a token', function(done) {
        
        var data = {
            card : cards.visa1,
            customId : "MyTestCard"
        }
        api.createToken(data, function(reply){
            should.exist(reply);
            should.exist(reply.status);
            should.equal(reply.status, 0);
            should.exist(reply.token);
            
            var newToken = reply.token;
            api.queryToken({
                "firstName": data.card.cardholder.firstName,
                "lastName" : data.card.cardholder.lastName,
                // "customId": data.customId,
                // "expiration" : data.card.expiration,
                // "cardType" : "Visa"
                
            }, function(reply){
                should.exist(reply);
                should.exist(reply.status);
                should.equal(reply.status, 0)
                
                should.exist(reply.results);
                should.equal(reply.results.length, 1);
                should.equal(newToken, reply.results[0].token);
                
                api.updateToken({
                    "token" : reply.results[0].token,
                    "customId" : "AnotherTestCard"
                }, function(reply){
                    should.exist(reply);
                    should.exist(reply.status);
                    should.equal(reply.status, 0);
                    
                    api.deleteToken(newToken, function(reply){
                        should.exist(reply);
                        should.exist(reply.status);
                        should.equal(reply.status, 0);
                        done();
                    });
                });
                
            });
            
            
        });
    });
   
    it('will fail when tries to delete a false token', function(done) {
        
        api.deleteToken("123456", function(reply){
            should.exist(reply);
            should.exist(reply.status);
            should.equal(reply.status, 1);
            done();
        })
    });
    
    
});