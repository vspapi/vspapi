var assert = require('assert');
var should = require('should');
var logger = require('winston');
var api = require('../vspapi.js');
var cards = require('./sampleCards.js');
var ctx = require('./sampleCtx.js');

api.config(ctx);


logger.level = "debug";

var mockOn = (process.env.MOCKON === 'true');
console.log("MOCKON: " + mockOn);

describe('Payment Test Suite', function() {
    this.timeout(15000);

    
    it('can process a refund using card data', function(done) {
        var data = {
            amount : 500.00,
            card : cards.visa2
        };
        
        api.processRefund(data, function(reply){
            should.exist(reply);
            should.exist(reply.status);
            should.equal(reply.status, 0);
            done();
        });
    });
    
    it('can void a transaction', function(done) {
        var data = {
            amount : 500.00,
            card : cards.visa2,
        };
        
        api.processPayment(data, function(reply){
            should.exist(reply);
            should.exist(reply.status);
            should.equal(reply.status, 0);
            
            var data2 = {
                referenceNumber : reply.referenceNumber,
                //transactionDate : reply.transactionDate,
                amount: "10000"
            }
            api.voidTransaction(data2, function(reply2) {
                
                should.exist(reply2);
                should.exist(reply2.status);
                should.equal(reply2.status, 0);
                should.equal(reply2.responseCode, "00");
                should.exist(reply2.receipt);
                done();
            });
        });
    });
    
    it('can process a l3 payment', function(done) {
        
        var data = {
            amount : 500.00,
            card : cards.amex,
            
            l3data : {
                purchaseOrderNum : "67548",
                orderDate : "20151004",
                amount : {
                    discount : 1256,
                },
                items : [
                    {
                        code : "SKY-12",
                        desc : "Sky Club - SR22T",
                        quantity : 25,
                        uom : "Hours",
                        unitCost : 200.00,
                        amount : 500000,
                        tax : 35500,
                        taxRate : 710
                    },
                    {
                        code : "SKY-13",
                        desc : "Sky Club - Instruction",
                        quantity : 10000,
                        uom : "Hours",
                        unitCost : 600000,
                        amount : 10000
                    }
                ]
            }
        };
        
        api.processPayment(data, function(reply){
            should.exist(reply);
            should.exist(reply.status);
            should.equal(reply.status, 0);
            should.equal(reply.avsResponse, "Y");
            should.equal(reply.cvvResponse, "Y");
            should.exist(reply.receipt);
            should.exist(reply.responseCode);
            console.log(reply.receipt);
            done();
        });
    });
    
    it('can process a simple payment', function(done) {
        
        var data = {
            amount : 500.00,
            card : cards.visa2
        };
        
        api.processPayment(data, function(reply){
            should.exist(reply);
            should.exist(reply.status);
            should.equal(reply.status, 0);
            done();
        });
    });
    
    it('will process a basic no-token present payment', function(done){
        var data = {
          card : cards.discover,
          customId : "TEST-Card"
        };
        api.createToken(data, function(reply){
            should.exist(reply);
            should.exist(reply.status);
            should.equal(reply.status, 0);
            should.exist(reply.token);
            
            var data = {
                token: reply.token,
                amount: 5000.00
            }
            
            api.processPayment(data, function(reply){
                should.exist(reply);
                should.exist(reply.status);
                should.equal(reply.status, 0);
                done();
            });
            
        });
    });
});