**VSPAPI** facilitates the use of the vSecure Processing web services within a Node application.  To use these services you will need to setup your own developer account at [vSP Developer Portal](http://www.vspdevelopers.com/) and sign a non-disclosure agreement.

# Setting Up a Context

Setting up the vspapi is very simple.  Once your have signed up with the developer portal you will be assigned your own GID and other assorted values which are used to setup the initail context for the api.

## Context Parameters
Property | Type | Required | Description |
:--------|:--------:|:--------:|:----------|
gid | String | ✔ | This value is unique to the developer.  One must be created for you by vSecure Processing.   |
platform | String |   | Optionally override the default "Buypass" platform value. |
url | String |  | Optionally override the default base URL for the vSP web services |
tid | String |  | Optionally set a Terminal ID ("01" by default) |
appId | String | ✔ | The ID of your application.  This value may be assigned to you by vSP |
userId | String | ✔ | The user ID of the developer as registered with vSP |

## Example Usage
```javascript
var api = require('vspapi');

var ctx = {
    gid:"xxxxxxxxxxxxxxx",
    appId:"xxxxx",
    userId:"info@someplace.com"
};

api.config(ctx);
```


# See Also

+ [api.createToken(data, callback)](https://bitbucket.org/vspapi/vspapi/wiki/Create%20Token)
+ [api.deleteToken(data, callback)](https://bitbucket.org/vspapi/vspapi/wiki/Delete%20Token)
+ [api.queryToken(data, callback)](https://bitbucket.org/vspapi/vspapi/wiki/Query%20Token)
+ [api.updateToken(data, callback)](https://bitbucket.org/vspapi/vspapi/wiki/Update%20Token)
+ [api.processPayment(data, callback)](https://bitbucket.org/vspapi/vspapi/wiki/Process%20Payment)
+ [api.voidTransaction(data, callback)](https://bitbucket.org/vspapi/vspapi/wiki/Void%20Transaction)
+ [api.processRefund(data, callback)](https://bitbucket.org/vspapi/vspapi/wiki/Process%20Refund)
+ [api.processAuth(data, callback)](https://bitbucket.org/vspapi/vspapi/wiki/Process%20Authorization)
+ [api.processAuthCancel(data, callback)](https://bitbucket.org/vspapi/vspapi/wiki/Process%20Authorization%20Cancellation)
+ [api.processCaptureOnly(data, callback)](https://bitbucket.org/vspapi/vspapi/wiki/Process%20Capture%20Only)