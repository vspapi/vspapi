module.exports = function(data) {
    // copy values from data over to this object
    for(var key in data) {
        this[key] = data[key];
    }

    this.type = _getCardType(this.number);    
}

function _getCardType(cnum) {
    var fchar = Number.parseInt(cnum.charAt(0));
    
    switch (fchar) {
        case 3 : // Amex or Dinners Club or Carte Blanche
            var amexPattern = /^3(4|7)\d{13}$/g;
            var dinnersPattern = /^3(0|6|8)\d{12}$/g;
            return (amexPattern.test(cnum)) ? "AMEX" : (dinnersPattern.test(cnum))? "DC" : "Unsupported";
        case 6 : // Discover
            var first8Digits = cnum.substring(0, 8);
            return (cnum.length == 16 && 
                (_inRange(first8Digits, "60110000", "60119999") ||
                _inRange(first8Digits, "65000000", "65999999") ||
                _inRange(first8Digits, "62212600", "62292599"))) ? "DSCV" : "Unsupported";
        case 5 : // Master Card
            var mcPattern = /^5[1-5]\d{14}$/g;
            return (mcPattern.test(cnum))? "MC" : "Unsupported";
        case 4 : // Visa
            return (cnum.length == 13 || cnum.length == 16)? "VISA" : "Unsupported";
        
    }
}

function _inRange(value, min, max) {
    value = Number.parseInt(value);
    min = Number.parseInt(min);
    max = Number.parseInt(max);
    
    return (value >= min && value <= max);
}

