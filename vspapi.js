var logger = require('winston');
var rest = require('restler');
var parser = require('./parser.js');
var decoder = require('./decoder.js');
  
module.exports = exports = new VSPAPI();

function VSPAPI() {
  this.createToken = _createToken;
  this.deleteToken = _deleteToken;
  this.queryToken = _queryToken;
  this.updateToken = _updateToken;
  this.updateTokenExpiration = _updateTokenExpiration;
  this.processPayment = _processPayment;
  this.voidTransaction = _voidTransaction;
  this.processRefund = _processRefund;
  this.processAuth = _processAuth;
  this.processAuthCancel = _processAuthCancel;
  this.processCaptureOnly = _processCaptureOnly;
  this.decode = _decodeResponseCode;
  this.decodeCvv = _decodeCvvCode;
  this.decodeAvs = _decodeAvsCode;
  this.config = _config;
}

function _config(ctx) {
  
  
  if (_notDefined(ctx["platform"]))
    ctx.platform = "Buypass";
    
  if (_notDefined(ctx["url"]))
    ctx.url = "http://dvrotsos2.kattare.com";
    
  if (_notDefined(ctx["tid"]))
    ctx.tid = "01";
    
  this.ctx = ctx;
    
  this.merchantData = {
    "UserId": this.ctx.userId,
    "GID": this.ctx.gid,
    "Platform": this.ctx.platform,
    "Tid": this.ctx.tid,
    "ApplicationId": this.ctx.appId
  };
}

// -----------------------------------------------------
//                    Create Token
// -----------------------------------------------------
function _createToken(data, callback) {
  
  var vreq = {
    "MerchantData": this.merchantData,
    "CreateToken": {
      "AccountNumber": data.card.number,
      "ExpirationMonth": data.card.expiration.month,
      "ExpirationYear": data.card.expiration.year,
      "CardHolderFirstName": data.card.cardholder.firstName,
      "CardHolderLastName": data.card.cardholder.lastName,
      "AvsZip": data.card.avs.zip,
      "CustomId": data.customId
    }
  };
  
  _post(this.ctx, '/vsg2/createtoken', vreq, callback);
}


// -----------------------------------------------------
//                    Delete Token
// -----------------------------------------------------
function _deleteToken(token, callback) {
  var vreq = {
    "MerchantData": this.merchantData,
    "DeleteToken": {
      "Token": token
    }
  };
  
  _post(this.ctx, '/vsg2/deletetoken', vreq, callback);
}

// -----------------------------------------------------
//                    Query Token
// -----------------------------------------------------
function _queryToken(data, callback) {
  var vreq = {
    "MerchantData": this.merchantData,
    "QueryToken": {
      "FirstName": data.firstName,
      "LastName": data.lastName,
    }
  };
  
  var QueryToken = vreq.QueryToken;
  if(_isDefined(data["customId"]))
    QueryToken["CustomId"] = data.customId;
    
  if(_isDefined(data["cardType"]))
    QueryToken["CardType"] = data.cardType;
    
  if(_isDefined(data["expiration"])) {
    if(_isDefined(data.expiration["month"]))
      QueryToken["ExpirationMonth"] = data.expiration.month;
    if(_isDefined(data.expiration["year"]))
      QueryToken["ExpirationYear"] = data.expiration.year;
  }
  
  _post(this.ctx, '/vsg2/querytoken', vreq, function(reply){
    if(reply.status === "0") {
      var results = new Array();
      if(_isArray(reply.data)) {
        for(var ndx=0; ndx<reply.data.length; ndx++) {
          results.push(_fixCase(reply.data[ndx]));
        }
      } else {
        results.push(_fixCase(reply.data));
      }
      
      callback({
        "status": reply.status,
        "results": results
      });
    } else {
      callback(reply);
    }
  });
}

// -----------------------------------------------------
//                Update Expiration Token
// -----------------------------------------------------
function _updateTokenExpiration(data, callback) {
  var vreq = {
    "MerchantData": this.merchantData,
    "QueryToken": {
      "Token": data.token,
      "ExpirationMonth": data.expiration.month,
      "ExpirationYear": data.expiration.year
    }
  };
  
  _post(this.ctx, '/vsg2/updateexpiration', vreq, callback);
}

// -----------------------------------------------------
//                    Update Token
// -----------------------------------------------------
function _updateToken(data, callback) {
  var vreq = {
    "MerchantData": this.merchantData,
    "UpdateToken": {
      "Token": data.token,
      "ApplicationId" : this.ctx.appId
    }
  };
  
  var UpdateToken = vreq.UpdateToken;
  
  if(_isDefined(data["customId"]))
    UpdateToken["CustomId"] = data.customId;
    
  if(_isDefined(data["expiration"])) {
    if(_isDefined(data.expiration["month"]))
      UpdateToken["ExpirationMonth"] = data.expiration.month;
    if(_isDefined(data.expiration["year"]))
      UpdateToken["ExpirationYear"] = data.expiration.year;
  }
    
  if(_isDefined(data["avs"])) {
    if(_isDefined(data.avs["zip"]))
      UpdateToken["AvsZip"] = data.avs.zip;
    if(_isDefined(data.avs["street"]))
      UpdateToken["AvsStreet"] = data.avs.street;
  }
  
  if(_isDefined(data["customId"]))
    UpdateToken["CustomId"] = data.customId;
  
  _post(this.ctx, '/vsg2/updatetoken', vreq, callback);
}

// -----------------------------------------------------
//                    Process Payment
// -----------------------------------------------------
function _processPayment(data, callback) {
  var vreq = {
    "MerchantData": this.merchantData,
    "ProcessPayment": {
      "Amount": data.amount,
      "ApplicationId": this.ctx.appId,
      "Recurring": 0
    }
  };
  
  var ProcessPayment = vreq.ProcessPayment;
  
  if (_isDefined(data["recurring"])) {
    ProcessPayment.Recurring = (data.recurring)? 1 : 0;
  }
  
  if (_notDefined(data.token)) {
    var card = data.card;
    ProcessPayment.AccountNumber = card.number;
    ProcessPayment.ExpirationMonth = card.expiration.month;
    ProcessPayment.ExpirationYear = card.expiration.year;
    ProcessPayment.Cvv = card.cvv;
    ProcessPayment.CardHolderFirstName = card.cardholder.firstName;
    ProcessPayment.CardHolderLastName = card.cardholder.lastName;
    ProcessPayment.AvsZip = card.avs.zip;
    ProcessPayment.AvsStreet = card.avs.street;
  } else {
    ProcessPayment.Token = data.token;
  }
  
  if (_isDefined(data["recurring"])) {
    ProcessPayment.Recurring = (data.recurring)? 1 : 0;
  }
  
  if (_isDefined(data["custom"])) {
    for (var ndx=0; ndx<data.custom.length; ndx++) {
      var field = data.custom[ndx];
      if(ndx == 3) break; 
      ProcessPayment["Cf"+(ndx+1)] = field;
    }
  }
  
  if (_isDefined(data["track"])) {
    for (var ndx=0; ndx<data.track.length; ndx++) {
      var tdata = data.track[ndx];
      if(ndx == 2) break; 
      ProcessPayment["Trk"+(ndx+1)] = tdata;
    }
  }
  
  if (_isDefined(data["ecommerce"])) {
    ProcessPayment["IndustryType"] = {
      "IndType": data.ecommerce.industryType,
      "IndInvoice": data.ecommerce.invoice
    };
  } else {
  
    if (_isDefined(data["l2data"])) {
      var l2data = data.l2data;
      ProcessPayment["Level2PurchaseInfo"] = {
        "Level2CardType": l2data.cardType,
        "PurchaseCode": l2data.purchaseCode,
        "ShipToCountryCode": l2data.shipping.countryCode,
        "ShipToPostalCode": l2data.shipping.postalCode,
        "ShipFromPostalCode": l2data.shipping.fromPostalCode,
        "SalesTax": l2data.salesTax
      }
      
      if (_isDefined(l2data["products"])) {
        for (var ndx=0; ndx<l2data.products.length; ndx++) {
          var product = l2data.products[ndx];
          if(ndx == 4) break; 
          ProcessPayment.Level2PurchaseInfo["ProductDescription"+(ndx+1)] = product;
        }
      }
    }
    
    if (_isDefined(data["l3data"])) {
      var l3data = data.l3data;
      ProcessPayment["Level3PurchaseInfo"] = {
        "PurchaseOrderNumber": l3data.purchaseOrderNum,
        "OrderDate": l3data.orderDate,
        "DutyAmount": _fixNum(l3data.amount.duty,2),
        "AlternateTaxAmount": _fixNum(l3data.amount.altTax,2),
        "DiscountAmount": _fixNum(l3data.amount.discount,2),
        "FreightAmount": _fixNum(l3data.amount.freight,2),
        "TaxExemptFlag": (l3data.taxExempt)? "True" : "False"
      }
      
      if (_isDefined(l3data["items"])) {
        ProcessPayment.Level3PurchaseInfo["LineItemCount"] = l3data.items.length;
        var ndx = 0;
        
        
        var LineItems = new Array();
        for (ndx=0; ndx<l3data.items.length; ndx++) {
          var item = l3data.items[ndx];
          LineItems.push({
            "ItemSequenceNumber": _npad(ndx+1,3),
            "ItemCode": item.code,
            "ItemDescription": item.desc,
            "ItemQuantity": _fixNum(item.quantity, 4),
            "ItemUnitOfMeasure": item.uom,
            "ItemUnitCode": item.unitCode,
            "ItemAmount": _fixNum(item.amount, 2),
            "ItemDiscountAmount": _fixNum(item.discount,2),
            "ItemTaxAmount": _fixNum(item.tax,2),
            "ItemTaxRate": _fixNum(item.taxRate,3),
          })
        }
        ProcessPayment.Level3PurchaseInfo.PurchaseItems = {
          "LineItem" : LineItems
        }
      }
    }
  
  }
  
  _post(this.ctx, '/vsg2/processpayment', vreq, callback);
}

// -----------------------------------------------------
//                 Void Transaction
// -----------------------------------------------------
function _voidTransaction(data, callback) {
  
  var vreq = {
    "MerchantData": this.merchantData,
    "ProcessVoid": {
      "Amount": _fixNum(data.amount,2),
      "UserId": this.ctx.userId,
      "ReferenceNumber": data.referenceNumber
    }
  };
  
  var ProcessVoid = vreq.ProcessVoid;
  
  if (_isDefined(data["token"])) {
    ProcessVoid["Token"] = data.token;
  } else if (_isDefined(data["card"])) {
    ProcessVoid["AccountNumber"] = data.card.number;
    ProcessVoid["ExpirationMonth"] = data.card.expiration.month;
    ProcessVoid["ExpirationYear"] = data.card.expiration.year;
  }
  
  if (_isDefined(data["transactionDate"])) {
    ProcessVoid["TransactionDate"] = data.transactionDate;
  }
  
  _post(this.ctx, '/vsg2/processvoid', vreq, callback);
  
}

// -----------------------------------------------------
//                 Process Refunds
// -----------------------------------------------------
function _processRefund(data, callback) {
  var vreq = {
    "MerchantData": this.merchantData,
    "ProcessRefund": {
      "Amount": data.amount,
      "ApplicationId": this.ctx.appId,
    }
  };
  
  var ProcessRefund = vreq.ProcessRefund;
  if (_isDefined(data["token"])) {
    ProcessRefund["Token"] = data.token;
  } else if (_isDefined(data["card"])) {
    ProcessRefund["AccountNumber"] = data.card.number;
    ProcessRefund["ExpirationMonth"] = data.card.expiration.month;
    ProcessRefund["ExpirationYear"] = data.card.expiration.year;
  }
  
  _post(this.ctx, '/vsg2/processrefund', vreq, callback);
}

// -----------------------------------------------------
//                 Process Authorization
// -----------------------------------------------------
function _processAuth(data, callback) {
  var vreq = {
    "MerchantData": this.merchantData,
    "ProcessAuth": {
      "Amount": data.amount,
      "ApplicationId": this.ctx.appId,
    }
  };
  
  var ProcessAuth = vreq.ProcessAuth;
  
  if (_notDefined(data.token)) {
    var card = data.card;
    ProcessAuth.AccountNumber = card.number;
    ProcessAuth.ExpirationMonth = card.expiration.month;
    ProcessAuth.ExpirationYear = card.expiration.year;
    ProcessAuth.Cvv = card.cvv;
    ProcessAuth.CardHolderFirstName = card.cardholder.firstName;
    ProcessAuth.CardHolderLastName = card.cardholder.lastName;
    ProcessAuth.AvsZip = card.avs.zip;
    ProcessAuth.AvsStreet = card.avs.street;
  } else {
    ProcessAuth.Token = data.token;
  }
  
  if (_isDefined(data["custom"])) {
    for (var ndx=0; ndx<data.custom.length; ndx++) {
      var field = data.custom[ndx];
      if(ndx == 3) break; 
      ProcessAuth["Cf"+(ndx+1)] = field;
    }
  }
  
  if (_isDefined(data["ecommerce"])) {
    ProcessAuth["IndustryType"] = {
      "IndType": data.ecommerce.industryType,
      "IndInvoice": data.ecommerce.invoice
    };
  }
  
  _post(this.ctx, '/vsg2/processauth', vreq, callback)
}

// -----------------------------------------------------
//              Process Authorization Cancel
// -----------------------------------------------------
function _processAuthCancel(data, callback) {
  var vreq = {
    "MerchantData": this.merchantData,
    "ProcessAuthCancel": {
      "Amount": data.amount,
      "ApplicationId": this.ctx.appId,
      "ReferenceNumber": data.referenceNumber,
      "AccountNumber": data.card.number,
      "ExpirationMonth": data.card.expiration.month,
      "ExpirationYear": data.card.expiration.year,
      "ResponseCode": data.responseCode,
      "TransactionDate": data.transactionDate
    }
  };
  
  if (_isDefined(data["ecommerce"])) {
    vreq.ProcessAuthCancel["IndustryType"] = {
      "IndType": data.ecommerce.industryType,
      "IndInvoice": data.ecommerce.invoice
    };
  }
  
  _post(this.ctx, '/vsg2/processauthcancel', vreq, callback)
}

// -----------------------------------------------------
//              Process Capture Only
// -----------------------------------------------------
function _processCaptureOnly(data, callback) {
  var vreq = {
    "MerchantData": this.merchantData,
    "ProcessPayment": {
      "Amount": data.amount,
      "ApplicationId": this.ctx.appId,
      "ReferenceNumber": data.referenceNumber,
      "AuthIdentificationResponse": data.authIdentificationResponse
    }
  };
  
  var ProcessPayment = vreq.ProcessPayment;
  
  if (_notDefined(data.token)) {
    var card = data.card;
    ProcessPayment.AccountNumber = card.number;
    ProcessPayment.ExpirationMonth = card.expiration.month;
    ProcessPayment.ExpirationYear = card.expiration.year;
    ProcessPayment.Cvv = card.cvv;
  } else {
    ProcessPayment.Token = data.token;
  }
  
  if (_isDefined(data["custom"])) {
    for (var ndx=0; ndx<data.custom.length; ndx++) {
      var field = data.custom[ndx];
      if(ndx == 3) break; 
      ProcessPayment["Cf"+(ndx+1)] = field;
    }
  }
  
  if (_isDefined(data["ecommerce"])) {
    ProcessPayment["IndustryType"] = {
      "IndType": data.ecommerce.industryType,
      "IndInvoice": data.ecommerce.invoice
    };
  }
  
  _post(this.ctx, '/vsg2/processcaptureonly', vreq, callback)
}

// -----------------------------------------------------
//            U T I L I T Y   M E T H O D S 
// -----------------------------------------------------
function _post(ctx, path, vreq, callback) {
  var xml = parser.toXml("Request", vreq);
  var url = ctx.url + path;
  
  rest.post(url, {
    multipart: true,
    data: {
      'param': xml
    }
  }).on('complete', function(data) {
    var reply = parser.toJson(data).Reply;
    
    callback(_fixCase(reply));
    
  }).on('error', function(err){
    
    logger.log('error', "!!! ERROR : " + url + " !!!");
    logger.log('error', err);
    callback({Status:1,ResponseMessage:err});
    
  }).on('fail', function(err){
    
    logger.log('error', "!!! FAIL : " + url + " !!!");
    logger.log('error', err);
    callback({Status:1,ResponseMessage:err});
    
  });
  
}

function _decodeResponseCode(code) {
  return decoder[code];
}

function _decodeCvvCode(cardtype, code) {
  return decoder.cvv[cardtype][code];
}

function _decodeAvsCode(cardtype, code) {
  return decoder.avs[cardtype][code];
}

function _notDefined(value) {
  return (typeof value === "undefined");
} 

function _isArray(value) {
  return (_isDefined(value))? value.constructor === Array : false;
}

function _isDefined(value) {
  return !_notDefined(value);
}

function _npad(number, size) {
  number = number.toString();
  while (number.length < size) number = "0" + number;
  return number;
}

function _fixCase(data) {
  _tweak(data, function(oldKey){
    return oldKey.charAt(0).toLowerCase() + oldKey.slice(1);
  });
  return data;
}

// tweak each property in an object
function _tweak(data, tweak) {
  var keys = Object.keys(data);
  
  for (var ndx=0; ndx<keys.length; ndx++) {
    var oldKey = keys[ndx];
    var newKey = tweak(oldKey);
    data[newKey] = data[oldKey];
    delete data[oldKey];
  }
}

function _fixNum(num, precision) {
  var float = parseFloat(num) + '';
  
  if (float == "NaN") return num; // do nothing
    
  float = float.split('.');
  
  if (float.length > 1) {
    var lnum = float[0];
    var rnum = float[1];
    
    if (rnum.length > precision) {
      num = lnum + rnum.substr(0, precision);
    } else {
      num = lnum + rnum + _zerofill(precision - rnum.length);
    }
  
  } else { 
    num = float[0] + _zerofill(precision);
  }
  
  return num;
}

function _zerofill(length) {
  var str = "";
  for (var count=0; count<length; count++) {
    str += "0";
  }
  return str;
}

