var xml2json = require('xml2json');
var json2xml = require('js2xmlparser');

module.exports = {
    toXml: function(root, data) {
        var options = {
          declaration: {
              include: false
          } ,
          prettyPrinting: {
              enabled: false
          }
        };
        return json2xml(root, data, options);
    },
    
    toJson: function(xml) {
        return xml2json.toJson(xml, {object: true});
    }
}