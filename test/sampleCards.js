
module.exports = {
    amex : {
        number: "370000000000002",
        expiration: {
           month: "01",
           year: "20"
        },
        cvv: "2546",
        cardholder: {
           firstName: "Barack",
           lastName: "Obama"
        },
        avs: {
           street:"1600 Pennsylvania Ave NW",
           zip:"20500"
        }
    },
    
    discover : {
        number: "6011000000000012",
        expiration: {
           month: "02",
           year: "22"
        },
        cvv: "2547",
        cardholder: {
           firstName: "Santa",
           lastName: "Klause"
        },
        avs: {
           street:"N Pole St",
           zip:"97877"
        }
    },
    
    visa1 : {
        number: "4007000000027",
        expiration: {
           month: "03",
           year: "23"
        },
        cvv: "2548",
        cardholder: {
           firstName: "Jessica",
           lastName: "Jones"
        },
        avs: {
           street:"1 Superman Square",
           zip:"62960"
        }
    },
    
    visa2 : {
        number: "4012888818888",
        expiration: {
           month: "04",
           year: "24"
        },
        cvv: "2549",
        cardholder: {
           firstName: "Bruce",
           lastName: "Wayne"
        },
        avs: {
           street:"28789 WI-60 Trunk",
           zip:"53540"
        }
    },
}