
var ProcessRefundSchema = {
	amount: Number,
	
	token: String,
	
	card: {
		number: String,
		expiration: {
			month: String,
			year: String
		} 
	}
}




var ProcessVoidSchema = {
	amount: Number,
	referenceNumber: String,
	// Identifies the transaction transmission date and time. Format: MMDDhhmmss
	transactionDate: Date,
	
	// Required when voiding an eCommerce or MOTO industry transactions. 
	// See section 3.2 for details.
	ecommerce: {
		industryType: String, // moto, ecom_1, ecom_2, ecom_3, ecom_4
		invoice: String, 
	},
	
	token: String,
	
	card: {
		number: String,
		expiration: {
			month: String,
			year: String
		}
	},
}


var ProcessPaymentSchema = {
	
	amount: Number,
	recurring: Boolean,

	custom: [String], //Cf1, Cf2, etc.

	
	// Only required for ecommerce or mail order transactions
	ecommerce: {
		industryType: String, // moto, ecom_1, ecom_2, ecom_3, ecom_4
		invoice: String, 
	},
	

	// Only required for Swipt Retail Transactions
	track: [String], // Optional

	// NOTE: Ignored if ecommerce specified
	l2data: {
		cardType: String,
		purchaseCode: String,
		shipping: {
			countryCode: String,
			postalCode: String,
			fromPostalCode: String
		},
		salesTax: String, // formatted as $$$$$cc - no decimal
		products: [String], // Max 4 items
	},

	// NOTE: Ignored if ecommerce specified
	l3data: {
		purchaseOrderNum: String,
		orderDate: Date, // Format YYYYMMDD
		amount: {
			duty: String, // Assumes 2 decimal places
			altTax: String, // Assumes 2 decimal places
			discount: String, // Assumes 2 decimal places
			freight: String, // Assumes 2 decimal places
		},
		taxExempt: Boolean, // "True" or "False"
		items: [{
			sequenceNum: Number, // Format "001" - created automatically
			code: String,
			desc: String,
			quantity: Number, // Assumes 4 decimal places
			uom: String,
			unitCost: Number, // Assumes 4 decimal places
			amount: Number, // Assumes 2 decimal places
			discount: Number, // Assumes 2 decimal places
			tax: Number, // Assumes 2 decimal places
			taxRate: Number // Assumes 2 decimal places
		}]
	},

	// !! Ignored if token specified.
	card: {
		number: String,
		expiration: {
			month: String,
			year: String
		},
		cvv: String,
		cardholder: {
			firstName: String,
			lastName: String
		},
		avs: {
			zip: String,
			street: String
		}
	},

	token: String,

	// NOT SUPPORTED
	userId: String, // Optional



}