module.exports = {
    
    "00" : "Approved or completed successfully",
    "01" : "Refer to card issuer",
    "02" : "Refer to card issuer’s special conditions",
    "03" : "Invalid merchant",
    "04" : "Pick up",
    "05" : "Do not honor",
    "06" : "Error",
    "08" : "Honor with identification",
    "10" : "Approved, partial amount approved",
    "11" : "Approved (VIP)",
    "12" : "Invalid transaction",
    "13" : "Invalid amount",
    "14" : "Invalid card number (no such number)",
    "15" : "No such issuer",
    "19" : "Try again",
    "21" : "Reversal error",
    "22" : "Reversal error",
    "25" : "Unable to locate record on file",
    "26" : "Referred ID not in DL database",
    "27" : "Referred - Call Center",
    "28" : "Referred skip trace info needed",
    "29" : "Hard negative info on file",
    "30" : "Format error (may also be a reversal)",
    "33" : "Expired card; pick up",
    "34" : "Suspected fraud; pick up",
    "36" : "Restricted card; pick up",
    "40" : "Total unavailable",
    "41" : "Lost card; pick up",
    "43" : "Stolen card; pick up",
    "51" : "Insufficient funds",
    "52" : "No checking account",
    "53" : "No savings account",
    "54" : "Expired card",
    "55" : "Incorrect PIN",
    "57" : "Transaction not permitted by card holder",
    "61" : "Exceeds withdrawal amount limit",
    "62" : "Restricted card",
    "65" : "Exceeds withdrawal frequency limit",
    "66" : "Card acceptor; call acquirer security",
    "67" : "Hard capture; pick up",
    "75" : "Allowable number of PIN tries exceeded",
    "76" : "Key synchronization error",
    "7U" : "Amount too large",
    "7V" : "Duplicate return",
    "7W" : "Unsuccessful",
    "7X" : "Duplicate reversal",
    "7Y" : "Subsystem unavailable",
    "7Z" : "Duplicate completion",
    "82" : "Count exceeds limit",
    "85" : "No reason to decline",  // NOTE: This response code is only applicable to AVS
                                    // Only Responses containing AVS Response Code “G” in 
                                    // Element No. 123 (AVS/Check Authorization Data). The 
                                    // Merchant/acquirer/ATM must use the AVS response 
                                    // provided in Field 123 to determine the appropriate action
    "86" : "Invalid card security code",
    "90" : "System error",
    "91" : "Issuer or switch is inoperative (time-out)",
    "92" : "Financial institution or intermediate network unknown for routing",
    "93" : "Transaction cannot be completed; violation of law",
    "94" : "Duplicate transaction",
    "96" : "System malfunction",
    "R0" : "Not approved",  // Used only in a Visa bill payment/recurring payment transaction.
                            // When this code is received, the merchant must not resubmit the 
                            // same transaction; however, the merchant may continue the billing 
                            // process in the subsequent billing period
                            
    "R1" : "Not approved",  // Used only in a Visa bill payment/recurring payment transaction.
                            // When this code is received, the merchant must stop recurring 
                            // payment requests.
        
   
    
    avs : {
        Amex : {
            "A" : "Only Address matches",
            "Z" : "Only postal code matches",
            "Y" : "Address and postal code matches",
            "N" : "Neither address nor postal code matches",
            "R" : "System unavailable",
            "U" : "Information unavailable",
            "S" : "Issuer does not support AVS"
        },
        DSCV : {
            "Y" : "Only Address matches",
            "Z" : "Only postal code matches",
            "A" : "Address and postal code matches",
            "N" : "Neither address nor postal code matches",
            "U" : "System unavailable, or Issuer does not support AVS",
            "W" : "Information unavailable",
            "G" : "International transaction - address not verified"
        },
        MC : {
            "A" : "Only Address matches",
            "Z" : "Only 5-digit postal code matches",
            "W" : "Only 9-digit postal code matches",
            "Y" : "Address and 5-digit postal code matches",
            "X" : "Address and 9-digit postal code matches",
            "N" : "Neither address nor postal code matches",
            "R" : "System unavailable",
            "U" : "Information unavailable",
            "S" : "Issuer does not support AVS"
        },
        VISA : {
            "A" : "Only Address matches",
            "Z" : "Only 5-digit postal code matches",
            "W" : "Only 9-digit postal code matches",
            "Y" : "Address and postal code matches",
            "N" : "Neither address nor postal code matches",
            "E" : "Ineligible transaction; or E message contains a content error",
            "R" : "System unavailable",
            "U" : "Information unavailable",
            "S" : "Issuer does not support AVS or AVS is no applicable",
            "C" : "Incompatible formats - Not verified",
            "P" : "Incompatible formats – Address P not verified; postal code matches",
            "F" : "United Kingdom – Address and F postal code match",
            "G" : "International transaction - address not verified",
            "I" : "International transaction - address not verified",
            "D" : "International transaction – Address and postal code match",
            "M" : "International transaction – Address and postal code match"
        },
    },
    
    cvv : {
        Amex : {
            "Y" : "Card identifier (CID) match",
            "N" : "CID non-match",
            "U" : "CID not checked",
            "X" : "Not Available"
        },
        DSCV : {
            "M" : "Card identifier (CID) match",
            "N" : "CID non-match",
            "P" : "Not processed",
            "S" : "CID should be on the card, but the merchant has indicated that it is not present",
            "U" : "Issuer is not certified",
            "X" : "Not Available"
        },
        MC : {
            "M" : "Valid or matched CVC2",
            "N" : "Invalid CVC2 value",
            "P" : "CVC2 not processed",
            "U" : "Issuer unregistered for CVC2",
            "X" : "Not Available"
        },
        VISA : {
            "M" : "CVV2 match",
            "N" : "CVV2 no match",
            "P" : "Not Processed",
            "S" : "CVV2 should be on the card, but the merchant has indicated that CVV2 is not present",
            "U" : "Issuer is not certified",
            "X" : "Not Available"
        },
    }
}