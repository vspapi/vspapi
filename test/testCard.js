var Card = require("../card.js");
var should = require('should');
var cards = require('./sampleCards.js');
var ctx = require('./sampleCtx.js');

describe('Token Test Suite', function() {
    
    
    
    it('Create Visa', function() {
        var testcard = cards.visa1;
        var testtype = "VISA";  
        
        var card = new Card(testcard);
        should.equal(card.number, testcard.number);
        should.equal(card.cvv, testcard.cvv);
        should.equal(card.cardholder.firstName, testcard.cardholder.firstName);
        should.equal(card.cardholder.lastName, testcard.cardholder.lastName);
        
        should.equal(card.expiration.month, testcard.expiration.month);
        should.equal(card.expiration.year, testcard.expiration.year);
        
        should.equal(card.avs.street, testcard.avs.street);
        should.equal(card.avs.zip, testcard.avs.zip);
        
        should.equal(card.type, testtype);
        
    });
    
    it('Create Discover', function() {
        var testcard = cards.discover;
        var testtype = "DSCV";  
        
        var card = new Card(testcard);
        should.equal(card.number, testcard.number);
        should.equal(card.cvv, testcard.cvv);
        should.equal(card.cardholder.firstName, testcard.cardholder.firstName);
        should.equal(card.cardholder.lastName, testcard.cardholder.lastName);
        
        should.equal(card.expiration.month, testcard.expiration.month);
        should.equal(card.expiration.year, testcard.expiration.year);
        
        should.equal(card.avs.street, testcard.avs.street);
        should.equal(card.avs.zip, testcard.avs.zip);
        
        should.equal(card.type, testtype);
        
    });
    
    it('Create American Express', function() {
        var testcard = cards.amex;
        var testtype = "AMEX";  
        
        var card = new Card(testcard);
        should.equal(card.number, testcard.number);
        should.equal(card.cvv, testcard.cvv);
        should.equal(card.cardholder.firstName, testcard.cardholder.firstName);
        should.equal(card.cardholder.lastName, testcard.cardholder.lastName);
        
        should.equal(card.expiration.month, testcard.expiration.month);
        should.equal(card.expiration.year, testcard.expiration.year);
        
        should.equal(card.avs.street, testcard.avs.street);
        should.equal(card.avs.zip, testcard.avs.zip);
        
        should.equal(card.type, testtype);
        
    });
    
});