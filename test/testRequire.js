
var ctx = require('./sampleCtx.js');
var api = require('../vspapi.js');
var doTest = require('./sampleModule.js');

api.config(ctx);

describe('Run within a Module', function() {
    this.timeout(15000);
    
    it('can auth then cancel', function(done){
        doTest(done);
    });
    
});