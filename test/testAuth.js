var assert = require('assert');
var should = require('should');
var logger = require('winston');
var api = require('../vspapi.js');
var cards = require('./sampleCards.js');
var ctx = require('./sampleCtx.js');

api.config(ctx);
logger.level = "debug";


describe('Authorize Test Suite', function() {
    this.timeout(15000);
    
    it('can auth then cancel', function(done){
        
        var data = {
            amount : 500.00,
            card : cards.visa2
        };
        
        api.processAuth(data, function(reply){
            should.exist(reply);
            should.exist(reply.status);
            should.equal(reply.status, 0);
            
            should.exist(reply.referenceNumber);
            data.referenceNumber = reply.referenceNumber;
            
            should.exist(reply.responseCode);
            data.responseCode = reply.responseCode;
            
            should.exist(reply.transactionDate);
            data.transactionDate = reply.transactionDate;
            
            api.processAuthCancel(data, function(reply){
                should.exist(reply);
                should.exist(reply.status);
                should.equal(reply.status, 0);
                done();
            });
        });
    });
    
    it('can auth then capture', function(done){
        
        var data = {
            amount : 500.00,
            card : cards.visa2
        };
        
        api.processAuth(data, function(reply){
            should.exist(reply);
            should.exist(reply.status);
            should.equal(reply.status, 0);
            
            should.exist(reply.referenceNumber);
            data.referenceNumber = reply.referenceNumber;
            
            should.exist(reply.responseCode);
            data.responseCode = reply.responseCode;
            
            should.exist(reply.authIdentificationResponse);
            data.authIdentificationResponse = reply.authIdentificationResponse;
            
            api.processCaptureOnly(data, function(reply){
                should.exist(reply);
                should.exist(reply.status);
                should.equal(reply.status, 0);
                done();
            });
        });
    });
    
    
});